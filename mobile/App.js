import React from 'react';
import { AppRegistry, View } from 'react-native'

import allReducers from './reducers/allReducers.js';
import MainApp from './MainApp'

import { Provider } from 'react-redux';
import { createStore } from 'redux';


const store = createStore(allReducers);



export default class App extends React.Component {
  render() {
    return(
      <Provider store={store}>
          <MainApp/>
      </Provider>
    );
  }
}
