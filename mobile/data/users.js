export default function () {
    return [
        {
            id: 1,
            fullName: "Рябов Максим",
            login: "kek_everyday",
            password: "123",
            role: "student"
        },
        {
            id: 2,
            fullName: "Иванов Иван",
            login: "ivan",
            password: "123",
            role: "student"
        },
        {
            id: 3,
            fullName: "Петров Петр",
            login: "petr",
            password: "123",
            role: "admin"
        }
    ]
}
