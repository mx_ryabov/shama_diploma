import { Alert } from 'react-native';
import {getRequest, postRequest} from "../helpers.js";


export const types = {
  AUTH: 'AUTH',
  AUTH_SUCCESS: 'AUTH_SUCCESS',
  AUTH_ERROR: 'AUTH_ERROR',

  SELECT_DATE: 'SELECT_DATE',
  SELECT_TIME_LINE: 'SELECT_TIME_LINE',

  FETCH_ROOMS: 'FETCH_ROOMS',
  FETCH_ROOMS_SUCCESS: 'FETCH_ROOMS_SUCCESS',
  FETCH_ROOMS_ERROR: 'FETCH_ROOMS_ERROR',

  FETCH_TIME_LINES: 'FETCH_TIME_LINES',
  FETCH_TIME_LINES_SUCCESS: 'FETCH_TIME_LINES_SUCCESS',
  FETCH_TIME_LINES_ERROR: 'FETCH_TIME_LINES_ERROR',

  ENROLL_COWORKING: 'ENROLL_COWORKING',
  ENROLL_SUCCESS: 'ENROLL_SUCCESS',
  ENROLL_ERROR: 'ENROLL_ERROR',

  UNENROLL_COWORKING: 'UNENROLL_COWORKING',

  FETCH_SCHEDULES_ROOM: 'FETCH_SCHEDULES_ROOM'
}

// Helper functions to dispatch actions, optionally with payloads
export const actionCreators = {
  auth: (login, password) => {
      let req = postRequest( "/users/login", { login: login, password: password });
      return { type: types.AUTH, payload: req }
  },
  authSuccess: (me) => {
      return { type: types.AUTH_SUCCESS, payload: me }
  },
  authError: (error) => {
      return { type: types.AUTH_ERROR, payload: error }
  },


  selectDay: (day) => {
      return { type: types.SELECT_DATE, payload: day }
  },
  selectTimeLine: (time) => {
      return { type: types.SELECT_TIME_LINE, payload: time }
  },


  fetchTimeLines: (date) => {
      let req = getRequest("/cow/rooms/get_by", {date: date.toLocaleDateString()});
      return { type: types.FETCH_TIME_LINES, payload: req }
  },
  fetchTimeLinesSuccess: (time_lines) => {
      Object.keys(time_lines.data).forEach((key) => { time_lines.data[key].selected = false; })
      return { type: types.FETCH_TIME_LINES_SUCCESS, payload: time_lines }
  },
  fetchTimeLinesError: (error) => {
      return { type: types.FETCH_TIME_LINES_ERROR, payload: error }
  },


  fetchAllRooms: () => {
      let req = getRequest("/cow/rooms/get_all", {});
      return { type: types.FETCH_ROOMS, payload: req }
  },
  fetchAllRoomsSuccess: (rooms) => {
      return { type: types.FETCH_ROOMS_SUCCESS, payload: rooms }
  },
  fetchAllRoomsError: (error) => {
      return { type: types.FETCH_ROOMS_ERROR, payload: error }
  },


  enrollCoworking: (date, time, coworking) => {
      let req = postRequest("/cow/schedule/add", {date: date, time: time, room: coworking._id});
      return { type: types.ENROLL_COWORKING, payload: req }
  },
  enrollCoworkingSuccess: (coworking_id) => {
      return { type: types.ENROLL_SUCCESS, payload: coworking_id }
  },
  enrollCoworkingError: (error) => {
      return { type: types.ENROLL_ERROR, payload: error }
  },


  unenrollCoworking: (schedule_id) => {
      let req = postRequest("/cow/schedule/remove", {schedule_id});
      return { type: types.UNENROLL_COWORKING, payload: req }
  },

  fetchSchedulesRoomByDate: (date, room_id) => {
      let req = getRequest("/cow/schedule", {date, room_id});
      return { type: types.FETCH_SCHEDULES_ROOM, payload: req }
  }
}
