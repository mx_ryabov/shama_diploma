import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Alert, ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { StackNavigator, createStackNavigator } from 'react-navigation';
import { Bars } from 'react-native-loader';


import AccordionListCoworking from './components/AccordionListCoworking.js';
import HallScreen from './HallScreen.js';
import ScheduleScreen from './ScheduleScreen.js';
import {getRequest} from '../../helpers.js';
import { actionCreators } from '../../actions/allActions.js';




class CoworkingList extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchAllRooms();
    }

    goToHallScreen(hall) {
        this.props.navigation.navigate("HallScreen", hall);
    }

    render() {
        let {upi, urgu, loading, error} = this.props.schedule.halls;
        let count = upi.length + urgu.length;

        if (loading) {
          return (
              <View style={styles.loadingContainer}>
                  <Bars size={16} color="#E35D5D" />
              </View>
          )
        }
        return (
            <View style={styles.mainContainer}>
                <View style={styles.headerContainer}>
                    <View style={styles.header}>
                        <Text style={[styles.text, {fontSize: 18}]}>Коворкинги</Text>
                        <Text style={styles.text}>{count} всего</Text>
                    </View>
                </View>
                <ScrollView style={styles.scrollViewContainer}>
                    <AccordionListCoworking
                        upi={upi}
                        urgu={urgu}
                        checkOnMyEnroll={false}
                        handleTouchOnCell={this.goToHallScreen.bind(this)}
                    />
                </ScrollView>
            </View>
        );
    }
}


const mapStateToProps = (state) => ({
    schedule: state.schedule,
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchAllRooms: () => {
            dispatch(actionCreators.fetchAllRooms()).payload.then((response) => {
                !response.error ? dispatch(actionCreators.fetchAllRoomsSuccess(response.data)) : dispatch(actionCreators.fetchAllRoomsError(response.data));
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoworkingList)

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  header: {
    alignItems: 'center',
    paddingTop: 25,
    paddingBottom: 10,
  },
  text: {
    color: "#A2A2A2",
  },
  mainContainer: {
    backgroundColor: "white",
    flex: 1
  },
  headerContainer: {
    backgroundColor: 'white',
    paddingLeft: 20,
    paddingRight: 20,
  },
  scrollViewContainer: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  headerAccordion: {
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 5,
    borderColor: "lightgray"
  },
  cell: {
    width: "50%",
  },
  rowCell: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: -10,
    marginRight: -10,
  }
});
