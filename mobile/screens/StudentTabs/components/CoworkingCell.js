import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity
} from 'react-native';
import { LinearGradient } from 'expo';

export default class CoworkingCell extends Component {
    constructor(props) {
        super(props);
    }

    goToHallScreen() {
        this.props.navigation.navigate("HallScreen", this.props.hall);
    }

    render() {
        let colors = this.props.isEmpty ? ["#f6d365", "#fda085"] : ["#b0b0b0", "#878787"];
        let bottomText = this.props.isMy ? `Забронирована вами` : `Вместимость: ${this.props.hall.capacity}`;
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.props.onPress.bind(null, this.props.hall, this.props.isMy)}>
                    <LinearGradient
                        start={[0.0, 1.0]}
                        end={[1.0, 0.0]}
                        colors={colors}
                        style={styles.coworkingCellNotEmpty}
                    >
                        <View>
                            <Text style={{color: "#fff", fontSize: 20, marginBottom: 10}}>{this.props.hall.corpus.name}-{this.props.hall.number}</Text>
                        </View>
                        <View>
                            <Text style={{color: "#fff"}}>{bottomText}</Text>
                        </View>
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );

    }
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  coworkingCell: {
    alignItems: "center",
    borderRadius: 5,
    elevation: 2,
    paddingTop: 20,
    paddingBottom: 20,
    marginBottom: 3
  },
  coworkingCellNotEmpty: {
    alignItems: "center",
    borderRadius: 5,
    elevation: 2,
    paddingTop: 20,
    paddingBottom: 20,
    marginBottom: 3
  },
  lectureHallCircle: {
    backgroundColor: "#F2F3F7",
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    marginBottom: 10
  },
  lectureHallNumber: {
    color: "#b9c1c8",
    fontSize: 15
  }
});
