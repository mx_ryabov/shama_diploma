import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Alert, TouchableOpacity
} from 'react-native';
import { LinearGradient } from 'expo';
import { connect } from 'react-redux';

import { data_calendar } from '../../../data/calendar.js';
import Day from './DayForCalendar.js';
import { actionCreators } from '../../../actions/allActions.js';



class Calendar extends Component {
  constructor(props) {
      super(props);
      let currentDate = new Date();
      this.weeks = data_calendar.getWeeks(currentDate.getFullYear(), currentDate.getMonth());

      this.state = {
          currentMonth: data_calendar.months[currentDate.getMonth()],
          currentNumberMonth: currentDate.getMonth(),
          currentYear: currentDate.getFullYear(),
          activeDay: new Date()
      }
  }

  _accessToDay(date) {
      return date.date.getMonth() === this.state.currentNumberMonth ? date.date.getDate() >= new Date().getDate() : false;
  }

  selectDay(day) {
      this.setState({activeDay: day})
      this.props.dispatch(actionCreators.selectDay(day));
      this.props.updateTimeLine(day);
  }



  render() {
    const headerDaysOfWeek = data_calendar.daysOfWeek.map((day, ind) => {
        return (
            <View style={styles.dayStyle} key={ind}>
                <Text style={{color: "#fff", fontSize: 16}}>{day}</Text>
            </View>
        )
    });

    const datesOfMonth = this.weeks.map((week, indWeek) => {
        const datesOfWeek = week.map((date, indDate) => {
            return (
                <Day
                    key={indDate}
                    enabled={this._accessToDay.bind(this, date)()}
                    date={date.date}
                    selected={date.date.getDate() === this.state.activeDay.getDate()}
                    onPress={this.selectDay.bind(this, date.date)}
                />
            )
        })

        return (
            <View key={indWeek} style={styles.daysOfWeek}>
                {datesOfWeek}
            </View>
        )
    })

    return (
      <View style={styles.container}>
          <LinearGradient
              start={[0.0, 1.0]}
              end={[1.0, 0.0]}
              colors={["#f6d365", "#fda085"]}
              style={styles.backgroundGradient}
          >
              <Text style={styles.headerText}>Календарь</Text>
              <Text style={styles.nameMonth}>{this.state.currentMonth}</Text>
              <View style={styles.days}>

                  <View>
                      <View style={styles.daysOfWeek}>
                          {headerDaysOfWeek}
                      </View>
                      {datesOfMonth}
                  </View>
              </View>
          </LinearGradient>
      </View>
    );
  }
}


const mapStateToProps = (state) => ({
    schedule: state.schedule
});

export default connect(mapStateToProps)(Calendar)


const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  backgroundGradient: {
    width: "100%",
    alignItems: "center",
    paddingTop: 30,
    paddingBottom: 20
  },
  headerText: {
    color: "#fff",
    fontSize: 16
  },
  nameMonth: {
    color: "#fff",
    fontSize: 20,
    marginTop: 15,
    marginBottom: 10
  },
  days: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  daysOfWeek: {
    justifyContent: "space-around",
    flexDirection: "row",
    width: "100%",
    marginTop: 12
  },
  enabledDay: {
    color: "#fff",
  },
  disabledDay: {
    color: "rgba(255, 255, 255, .4)"
  },
  dayStyle: {
    flex: 1,
    alignItems: "center"
  }
});
