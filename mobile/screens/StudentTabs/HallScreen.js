import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity, Dimensions, Image, Alert
} from 'react-native';
import Carousel from 'react-native-banner-carousel';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { LinearGradient } from 'expo';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 260;


export default class HallScreen extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            currentHall: this.props.navigation.state.params,
            images: [
                "https://images.pexels.com/photos/584399/living-room-couch-interior-room-584399.jpeg?auto=compress&cs=tinysrgb&h=350",
                "http://cdn22.urzadzamy.smcloud.net/t/photos/t/63414/maly-salon-w-jasnych-barwach_2899066.jpg",
                "http://alternativas.info/wp-content/uploads/2018/04/free-living-room-photos-lovely-free-living-room-and-royalty-free-stock-s.jpg"
            ]
        }
    }

    _renderPage(image, index) {
        return (
            <View key={index}>
                <Image style={{ width: BannerWidth, height: BannerHeight }} source={{ uri: image }} />
            </View>
        );
    }

    _bookIt() {
        this.props.navigation.navigate("ScheduleScreen");
    }



    render() {
        let {number, capacity} = this.state.currentHall;
        let {address, name: block} = this.state.currentHall.corpus;
        return (
            <View style={styles.container}>

                <View>
                    <View style={styles.headerAbsoluteContainer}>
                        <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={styles.backButton}>
                            <Ionicons name="ios-arrow-round-back" size={32} color={"#fff"} />
                        </TouchableOpacity>
                        <View style={styles.containerHeaderTitle}>
                            <Text style={styles.headerTitle}>{`${block}-${number}`}</Text>
                        </View>
                        <View style={{flex: 2}}></View>
                    </View>
                    <Carousel
                        autoplay
                        autoplayTimeout={5000}
                        loop
                        index={0}
                        pageSize={BannerWidth}
                        pageIndicatorStyle={{backgroundColor: "white"}}
                        activePageIndicatorStyle={{backgroundColor: "#E35D5D"}}
                    >
                        {this.state.images.map((image, index) => this._renderPage(image, index))}
                    </Carousel>
                </View>
                <View style={styles.infoContainer}>
                    <Text style={{marginBottom: 10}}>Адрес: {address}</Text>
                    <Text>Вместимость: {capacity}</Text>

                </View>
                <TouchableOpacity onPress={this._bookIt.bind(this)} style={styles.positionBookItButton}>
                    <LinearGradient
                        start={[0.0, 1.0]}
                        end={[1.0, 0.0]}
                        colors={["#f6d365", "#fda085"]}
                        style={styles.bookItButton}
                    >
                        <MaterialIcons name="schedule" size={32} color={"#fff"} />
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  headerAbsoluteContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    zIndex: 1000,
    height: 70,
    paddingTop: 20,
    backgroundColor: "rgba(0,0,0,0)",
    flexDirection: "row",
    width: "100%"
  },
  containerHeaderTitle: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 6
  },
  headerTitle: {
    color: "white",
    fontSize: 18,
  },
  infoContainer: {
    paddingTop: 20,
    paddingLeft: 30,
    paddingRight: 30
  },
  backButton: {
    height: 50,
    width: 50,
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bookItButton: {
    elevation: 2,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    margin: 2,
  },
  positionBookItButton: {
    position: "absolute",
    right: 20,
    bottom: 20
  }
});
