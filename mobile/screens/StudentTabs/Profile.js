import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

import AwesomeButton from '../../based_components/AwesomeButton.js';
import AwesomeTextInput from '../../based_components/AwesomeTextInput.js';



export default class Profile extends Component {
  constructor(props) {
      super(props);
      this.state = {
          firstName: "",
          lastName: "",
          email: "",
          phoneNumber: ""
      }
  }

  submitForm(navigation) {

  }

  render() {
    return (
      <View style={styles.container}>
          <View style={styles.headerContainer}>
              <TouchableOpacity style={styles.backButton}>
                  <Ionicons name="ios-arrow-round-back" size={32} color={"#fff"} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.backButton}>
                  <SimpleLineIcons name="logout" size={18} color={"#fff"} />
              </TouchableOpacity>
          </View>
          <View style={styles.inputsContainer}>
              <Text style={styles.headerText}>Настройки профиля</Text>
              <AwesomeTextInput
                  placeholder='Имя'
                  value={this.state.firstName}
                  onChangeText = { (firstName) => { this.setState({firstName}) } }
              />
              <AwesomeTextInput
                  placeholder='Фамилия'
                  value={this.state.lastName}
                  onChangeText = { (lastName) => { this.setState({lastName}) } }
              />
              <AwesomeTextInput
                  placeholder='Почта'
                  value={this.state.email}
                  onChangeText = { (email) => { this.setState({email}) } }
              />
              <AwesomeTextInput
                  placeholder='Номер телефона'
                  value={this.state.phoneNumber}
                  onChangeText = { (phoneNumber) => { this.setState({phoneNumber}) } }
              />
              <AwesomeButton
                  submitForm={this.submitForm.bind(this, this.props.navigation)}
                  text='Сохранить'
              />
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  inputsContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10
  },
  headerContainer: {
    height: 70,
    width: "100%",
    backgroundColor: "#E35D5D",
    paddingTop: 20,
    elevation: 2,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  backButton: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerText: {
    color: "#A2A2A2",
    fontSize: 24,
    marginBottom: 15,
    fontWeight: "100"
  }
});
