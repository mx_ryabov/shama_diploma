import React, { Component } from 'react';
import {
  View, Text, StyleSheet, TouchableOpacity
} from 'react-native';
import Modal from 'react-native-modal';
import { LinearGradient } from 'expo';



export default class EnrollModal extends Component {

    _renderModalInfoContent() {
        let hall = this.props.hall;
        let capacity = address = roomName = "";
        if (Object.keys(hall).length > 0) {
            capacity = hall.capacity;
            address = hall.corpus.address;
            roomName = `${hall.corpus.name}-${hall.number}`
        }
        const rows = [
            { name: "Вместительность:", value: capacity },
            { name: "Адрес:", value: address },
            { name: "Аудитория:", value: roomName }
        ]
        return rows.map((row, key) => {
            return (
                <View style={styles.rowInfo} key={key}>
                    <Text>{row.name}</Text>
                    <Text>{row.value}</Text>
                </View>
            )
        })
    }

    _renderModalContent() {
        return (
            <View style={styles.modalContent}>
                <View style={styles.contentContainer}>
                    <Text style={{color: "#A2A2A2", fontSize: 18, marginBottom: 20}}>Свободно!</Text>
                    <View style={{justifyContent: "center", width: "100%"}}>
                        {this._renderModalInfoContent.bind(this)()}
                    </View>
                </View>
                <View style={{flexDirection: "row", width: "100%", height: 50}}>
                    <View style={styles.containerButton}>
                        <TouchableOpacity onPress={this.props.onEnroll}>
                            <LinearGradient start={[0.0, 1.0]}
                                            end={[1.0, 0.0]}
                                            colors={["#f6d365", "#fda085"]}
                                            style={styles.enrollButton}>
                              <Text style={styles.text}>Записаться</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerButton}>
                        <TouchableOpacity style={styles.cancelButton} onPress={this.props.onClose}>
                            <Text style={{color: "#b9c1c8"}}>Отмена</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }


    render() {
        return (
            <Modal
                isVisible={this.props.isVisible}
                onBackdropPress={this.props.onClose}
                onBackButtonPress={this.props.onClose}
                animationIn={'slideInUp'}
                animationOut={'slideOutDown'}
                style={styles.modalStyle}
            >
                {this._renderModalContent()}
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
  text: {
    color: '#fff'
  },
  cancelButton: {
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    backgroundColor: "#F2F3F7",
  },
  containerButton: {
    width: "50%",
    height: "100%",
  },
  enrollButton: {
    alignItems: "center",
    justifyContent: "center",
    height: 50
  },
  contentContainer: {
    padding: 40,
    paddingTop: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  modalStyle: {
    margin: 0,
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%"
  },
  modalContent: {
    backgroundColor: 'white',
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  rowInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
  }
});
