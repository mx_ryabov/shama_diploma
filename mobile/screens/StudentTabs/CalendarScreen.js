import React, { Component } from 'react';
import {
  View, Text, StyleSheet, Alert, ScrollView, TouchableOpacity, CheckBox, RefreshControl
} from 'react-native';
import { connect } from 'react-redux';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { LinearGradient } from 'expo';
import { Bars } from 'react-native-loader';

import Calendar from './components/Calendar.js';
import { actionCreators } from '../../actions/allActions.js';



const mapStateToProps = (state) => ({
    schedule: state.schedule,
});

class CalendarScreen extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false
        }
    }

    componentDidMount() {
        this.props.fetchTimeLines(this.props.schedule.currentDateCalendar);
    }

    selectTime(time) {
        this.props.selectTimeLine(time);
    }

    _renderTimeList() {
        let countEmptyInTheDay = 0;
        let time_line = this.props.schedule.time_line.data;
        let {loading} = this.props.schedule.time_line;

        if (loading) {
          return (
              <View style={styles.loadingContainer}>
                  <Bars size={16} color="#E35D5D" />
              </View>
          )
        }

        return Object.keys(time_line).map((time, ind) => {
            let hours = parseInt(time.split(":")[0]),
                minutes = parseInt(time.split(":")[1]),
                nowHours = new Date().getHours(),
                nowMinutes = new Date().getMinutes();
                
            if ((hours*60 + minutes) < (nowHours*60 + nowMinutes) &&
                this.props.schedule.currentDateCalendar.toDateString() === new Date().toDateString()) return;

            //countEmptyInTheDay += time_line[time].upi.length + time_line[time].urgu.length;
            let upiEmpty = time_line[time].upi.filter((room) => {
                return room.renter === "";
            }).length;
            let urguEmpty = time_line[time].urgu.filter((room) => {
                return room.renter === "";
            }).length;

            countEmptyInTheDay += upiEmpty + urguEmpty;

            if (time_line[time].upi.length + time_line[time].urgu.length > 0) {
                return(
                    <TouchableOpacity onPress={this.selectTime.bind(this, time)} key={ind} style={styles.oneLine}>
                        <View style={styles.time}>
                            <Text>{time}</Text>
                            <View style={styles.checkBoxContainer}>
                                <CheckBox style={styles.checkBox} value={time_line[time].selected} onValueChange={this.selectTime.bind(this, time)}/>
                            </View>
                        </View>
                        <View style={styles.countEmpty}>
                            <Text>УПИ: {upiEmpty}</Text>
                            <Text style={{paddingLeft: 20}}>УргУ: {urguEmpty}</Text>
                        </View>
                    </TouchableOpacity>
                )
            }

        })
    }

    _goToSelectRoomScreen() {
        let time_line = this.props.schedule.time_line.data;
        let selectTimeLine = [];
        Object.keys(time_line).forEach((time) => {
            if (time_line[time].selected) selectTimeLine.push({...time_line[time], time: time});
        });

        this.props.navigation.navigate(
            "SelectRoom",
            {
                timeLine: selectTimeLine,
                date: this.props.schedule.currentDateCalendar,
                fetchTimeLines: this.props.fetchTimeLines,
                currentTimeIndex: 0
            }
        );
    }

    _onRefresh() {
        this.setState({refreshing: true});
        this.props.fetchTimeLines(this.props.schedule.currentDateCalendar, this.setState({refreshing: false}));
    }


    render() {
        let time_line = this.props.schedule.time_line.data;
        const isVisibleNextButton = Object.keys(time_line).filter(key => time_line[key].selected).length > 0;

        return (
          <View style={{flex: 1}}>
              <ScrollView
                  style={styles.container}
                  refreshControl={
                      <RefreshControl
                          refreshing={this.state.refreshing}
                          onRefresh={this._onRefresh.bind(this)}
                      />
                  }
                  >
                  <Calendar updateTimeLine={this.props.fetchTimeLines.bind(this)} />
                  <View>
                      <View style={styles.headerList}>
                          <Text style={styles.headerListText}>Время</Text>
                          <Text style={[styles.headerListText, {paddingLeft: 40}]}>Свободные коворкинги</Text>
                      </View>
                      <View style={styles.timeList}>
                          {this._renderTimeList.bind(this)()}
                      </View>
                  </View>
              </ScrollView>
              <TouchableOpacity onPress={this._goToSelectRoomScreen.bind(this)} style={styles.positionNextButton}>
                  <LinearGradient
                      start={[0.0, 1.0]}
                      end={[1.0, 0.0]}
                      colors={["#f6d365", "#fda085"]}
                      style={[styles.nextButton, {display: isVisibleNextButton ? "flex" : "none"}]}
                  >
                      <MaterialIcons name="navigate-next" size={32} color={"#fff"} />
                  </LinearGradient>
              </TouchableOpacity>
          </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchTimeLines: (date, callback) => {
            dispatch(actionCreators.fetchTimeLines(date)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.fetchTimeLinesSuccess(res)) :
                dispatch(actionCreators.fetchTimeLinesError(res));

                if (callback) callback();
            })
        },
        selectTimeLine: (time) => {
            dispatch(actionCreators.selectTimeLine(time));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarScreen);




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  headerList: {
    flexDirection: "row",
    paddingLeft: 20,
    paddingTop: 10
  },
  headerListText: {
    color: "#A2A2A2",
    fontSize: 16
  },
  timeList: {
    paddingLeft: 20,
    paddingRight: 20
  },
  oneLine: {
    flexDirection: "row",
    height: 40,
    alignItems: "center"
  },
  time: {
    flexDirection: "row",
    justifyContent: "flex-end",
    minWidth: 90
  },
  checkBoxContainer: {
    width: 20,
    height: 20,
    marginLeft: 10,
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center"
  },
  checkBox: {

  },
  countEmpty: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  nextButton: {
    elevation: 2,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    margin: 2,
  },
  positionNextButton: {
    position: "absolute",
    right: 20,
    bottom: 20
  },
  loadingContainer: {
    flex: 1,
    marginTop: 40,
    alignItems: "center",
    justifyContent: "center"
  }
});
