import React, { Component } from 'react';
import { StackNavigator, createStackNavigator } from 'react-navigation';

import CoworkingList from '../CoworkingList.js';
import HallScreen from '../HallScreen.js';
import ScheduleScreen from '../ScheduleScreen.js';


const CoworkingListStackNavigation = createStackNavigator({
    List: CoworkingList,
    HallScreen: HallScreen,
    ScheduleScreen: ScheduleScreen
});

export default class CoworkingListNavigation extends Component {
    render() {
        return <CoworkingListStackNavigation />
    }
}
