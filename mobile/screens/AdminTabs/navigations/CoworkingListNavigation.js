import React, { Component } from 'react';
import { StackNavigator, createStackNavigator } from 'react-navigation';

import CoworkingListScreen from '../CoworkingListScreen.js';
import CoworkingFormScreen from '../CoworkingFormScreen.js';
import CoworkingScreen from '../CoworkingScreen.js';


const AdminCoworkingListStackNavigation = createStackNavigator({
    CoworkingListScreen: CoworkingListScreen,
    CoworkingFormScreen: CoworkingFormScreen,
    CoworkingScreen: CoworkingScreen
});

export default class AdminCoworkingListNavigation extends Component {
    render() {
        return <AdminCoworkingListStackNavigation />
    }
}
