import React, { Component } from 'react';
import { StackNavigator, createStackNavigator } from 'react-navigation';

import AddNewAdminScreen from '../AddNewAdminScreen.js';


const AdminStackNavigation = createStackNavigator({
    AddNewAdminScreen: AddNewAdminScreen
});

export default class AdminScreenNavigation extends Component {
    render() {
        return <AdminStackNavigation />
    }
}
