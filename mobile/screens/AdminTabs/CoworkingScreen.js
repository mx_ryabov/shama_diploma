import React, { Component } from 'react';
import {
  View, Text, StyleSheet, ScrollView, TouchableOpacity
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { LinearGradient } from 'expo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Bars } from 'react-native-loader';


import { actionCreators } from '../../actions/allActions.js';
import InfoModal from './modals/InfoModal.js';



class CoworkingScreen extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            coworking: this.props.navigation.state.params.coworking,
            isOpenInfoModal: false,
            selectedTimeLine: {}
        }
        console.log('i am', this.state.coworking);
    }

    componentDidMount() {
        this.props.fetchSchedules("2018-8-14", this.state.coworking._id);
    }

    _showInfoModal(timeLine) {
        this.setState({selectedTimeLine: timeLine})
        this.setState({isOpenInfoModal: true})
    }

    _rendeScheduleByDate(date, listSchedule, ind) {
        const list = listSchedule.map((timeLine, ind) => {
            let fullName = `${timeLine.renter.firstName} ${timeLine.renter.lastName}`;
            return (
                <TouchableOpacity key={ind} style={styles.scheduleItem} onPress={this._showInfoModal.bind(this, timeLine)}>
                    <Text style={{minWidth: 50}}>{timeLine.time}</Text>
                    <Text>{fullName}</Text>
                    <Text>{timeLine.renter.phoneNumber}</Text>
                </TouchableOpacity>
            )
        });

        return (
            <View key={ind}>
                <View style={styles.headerScheduleByDate}>
                    <Text color={{color: "rgba(0,0,0,.5), fontSize: 18"}}>{date}</Text>
                </View>
                <View style={styles.listScheduleByDate}>
                    {list}
                </View>
            </View>
        )
    }

    _rendeScheduleByDates(data) {
        const dates = {};
        data.forEach((date) => {
            if (dates[date.date]) {
                dates[date.date].push(date)
            } else {
                dates[date.date] = [date];
            }
        });

        return Object.keys(dates).map((date, ind) => {
            return this._rendeScheduleByDate.bind(this)(date, dates[date], ind)
        })
    }

    goToCoworkingFormScreen(coworking) {
        this.props.navigation.navigate("CoworkingFormScreen", {coworking});
    }


    render() {
        let {coworking} = this.state,
            normalName = `${coworking.corpus.name}-${coworking.number}`;

        let {loading, data} = this.props.schedule.time_line;

        const list = [];

        if (loading) {
            return (
                <View style={styles.loadingContainer}>
                    <Bars size={16} color="#E35D5D" />
                </View>
            )
        }

        return (
            <View style={{flex: 1, backgroundColor: "#fff"}}>
                <ScrollView>
                    <InfoModal
                        isVisible={ this.state.isOpenInfoModal }
                        timeLine={this.state.selectedTimeLine}
                        onClose={ () => { this.setState({ isOpenInfoModal: false }) } } />

                    <View style={styles.container}>
                        <View style={styles.headerAbsoluteContainer}>
                            <TouchableOpacity onPress={() => {this.props.navigation.goBack()}} style={styles.backButton}>
                                <Ionicons name="ios-arrow-round-back" size={32} color={"rgba(0,0,0,.5)"} />
                            </TouchableOpacity>
                            <View style={styles.containerHeaderTitle}>
                                <Text style={styles.headerText}>{normalName}</Text>
                            </View>
                            <View style={{flex: 2}}></View>
                        </View>
                        <View style={{paddingLeft: 20, paddingRight: 20}}>
                              {this._rendeScheduleByDates.bind(this)(data)}
                        </View>


                    </View>
                </ScrollView>
                <TouchableOpacity onPress={this.goToCoworkingFormScreen.bind(this)} style={styles.positionNextButton}>
                    <LinearGradient
                        start={[0.0, 1.0]}
                        end={[1.0, 0.0]}
                        colors={["#E35D5D", "#E35D5D"]}
                        style={styles.nextButton}
                    >
                        <MaterialIcons name="edit" size={26} color={"#fff"} />
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    schedule: state.schedule
});

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSchedules: (date, room_id) => {
            dispatch(actionCreators.fetchSchedulesRoomByDate(date, room_id)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.fetchTimeLinesSuccess({data: res})) :
                dispatch(actionCreators.fetchTimeLinesError(res));
            });
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CoworkingScreen)


const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  nextButton: {
    elevation: 2,
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: "center",
    justifyContent: "center",
    margin: 2,
  },
  positionNextButton: {
    position: "absolute",
    right: 20,
    bottom: 20
  },
  headerScheduleByDate: {
      borderBottomWidth: 1,
      borderBottomColor: "rgba(0,0,0, .5)",
      paddingTop: 10,
      paddingBottom: 10

  },
  scheduleItem: {
      flexDirection: "row",
      paddingTop: 10,
      paddingBottom: 10,
      justifyContent: "space-between"
  },
  headerText: {
    fontSize: 20,
    color: "rgba(0,0,0, .5)"
  },
  backButton: {
    height: 50,
    width: 50,
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerAbsoluteContainer: {
    height: 70,
    paddingTop: 20,
    backgroundColor: "rgba(0,0,0,0)",
    flexDirection: "row",
    width: "100%",

  },
  containerHeaderTitle: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 6
  }
});
