export const types = {
  SELECT_DATE: 'SELECT_DATE',
  SELECT_TIME_LINE: 'SELECT_TIME_LINE',

  FETCH_ROOMS: 'FETCH_ROOMS',
  FETCH_ROOMS_SUCCESS: 'FETCH_ROOMS_SUCCESS',
  FETCH_ROOMS_ERROR: 'FETCH_ROOMS_ERROR',

  FETCH_TIME_LINES: 'FETCH_TIME_LINES',
  FETCH_TIME_LINES_SUCCESS: 'FETCH_TIME_LINES_SUCCESS',
  FETCH_TIME_LINES_ERROR: 'FETCH_TIME_LINES_ERROR',

  ENROLL_COWORKING: 'ENROLL_COWORKING',
  ENROLL_SUCCESS: 'ENROLL_SUCCESS',
  ENROLL_ERROR: 'ENROLL_ERROR',

  UNENROLL_COWORKING: 'UNENROLL_COWORKING',

  FETCH_SCHEDULES_ROOM: 'FETCH_SCHEDULES_ROOM'
}




// Initial state of the store
const initialState = {
  currentDateCalendar: new Date(),
  time_line: {
      data: {},
      loading: true
  },
  halls: {
      upi: [],
      urgu: [],
      loading: true
  },
  allRooms: []
}

export const scheduleReducer = (state = initialState, action) => {
    const {type, payload} = action

    switch (type) {
        case types.SELECT_DATE: {
            return {
                ...state,
                currentDateCalendar: payload
            }
        }

        case types.FETCH_ROOMS: {
            return {
                ...state,
                halls: {
                    upi: [],
                    urgu: [],
                    loading: true,
                    errors: null
                }
            }
        }

        case types.FETCH_ROOMS_SUCCESS: {
            return {
                ...state,
                halls: {
                    ...payload,
                    loading: false,
                    error: null
                }
            }
        }

        case types.FETCH_ROOMS_ERROR: {
            return {
                ...state,
                halls: {
                    upi: [],
                    urgu: [],
                    loading: false,
                    error: payload
                }
            }
        }

        case types.SELECT_TIME_LINE: {
            let timeLineData = Object.assign({}, state.time_line.data);
            timeLineData[payload].selected = !timeLineData[payload].selected;
            return {
                ...state,
                time_line: {
                    data: timeLineData,
                    loading: false,
                    error: null
                }
            }
        }

        case types.FETCH_TIME_LINES: {
            return {
                ...state,
                time_line: {
                    data: [],
                    loading: true,
                    errors: null
                }
            }
        }

        case types.FETCH_TIME_LINES_SUCCESS: {
            return {
                ...state,
                time_line: {
                    data: payload.data,
                    loading: false,
                    errors: null
                },
                allRooms: payload.rooms
            }
        }

        case types.FETCH_TIME_LINES_ERROR: {
            return {
                ...state,
                time_line: {
                    data: [],
                    loading: false,
                    errors: payload
                }
            }
        }

        case types.ENROLL_COWORKING: {
            return {
                ...state
            }
        }

        case types.ENROLL_SUCCESS: {

            return {
                ...state
            }
        }

        case types.ENROLL_ERROR: {
            return {
                ...state
            }
        }

        case types.UNENROLL_COWORKING: {
            return {
                ...state
            }
        }

        case types.FETCH_SCHEDULES_ROOM: {
            return {
                ...state,
                time_line: {
                    data: [],
                    loading: false,
                    errors: payload
                }
            }
        }
    }

    return state
}
