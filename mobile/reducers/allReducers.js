import {combineReducers} from 'redux';
import {userReducer} from './UserReducer';
import {scheduleReducer} from './ScheduleReducer';

const allReducers = combineReducers({
    user: userReducer,
    schedule: scheduleReducer
});

export default allReducers;
