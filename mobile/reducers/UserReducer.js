import users from '../data/users';

// The types of actions that you can dispatch to modify the state of the store
export const types = {

    AUTH: 'AUTH',
    AUTH_SUCCESS: 'AUTH_SUCCESS',
    AUTH_ERROR: 'AUTH_ERROR',

}



// Initial state of the store
const initialState = {
  me: {},
  loading: false
}

// Function to handle actions and update the state of the store.
// Notes:
// - The reducer must return a new state object. It must never modify
//   the state object. State objects should be treated as immutable.
// - We set \`state\` to our \`initialState\` by default. Redux will
//   call reducer() with no state on startup, and we are expected to
//   return the initial state of the app in this case.
export const userReducer = (state = initialState, action) => {
    const {users} = state
    const {type, payload} = action

    switch (type) {
        case types.AUTH: {
            return {
                ...state,
                me: {},
                loading: true
            }
        }

        case types.AUTH_SUCCESS: {
            return {
                ...state,
                me: payload,
                loading: false
            }
        }

        case types.AUTH_ERROR: {
            return {
                ...state,
                error: payload,
                loading: false
            }
        }
    }

    return state
}
