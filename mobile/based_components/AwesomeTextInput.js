import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

export default class AwesomeTextInput extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View>
          <TextInput
              underlineColorAndroid='#f6d365'
              selectionColor='#f6d365'
              placeholder={this.props.placeholder}
              style={styles.text_input}
              value={this.props.text}
              onChangeText = {this.props.onChangeText}/>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  text_input: {
    padding: 10,
    backgroundColor: 'white',
    borderRadius: 5,
    marginBottom: 15,
    width: '100%'
  }
});
