import React from 'react';
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { LinearGradient } from 'expo';

export default class AwesomeButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity onPress={ this.props.submitForm }>
        <LinearGradient start={[0.0, 1.0]}
                        end={[1.0, 0.0]}
                        colors={["#f6d365", "#fda085"]}
                        style={styles.buttonStyle}>
          <Text style={styles.text}>{this.props.text}</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}



const styles = StyleSheet.create({
  buttonStyle: {
    width: '100%',
    height: 45,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#ffffff'
  }
});
